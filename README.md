# Mid Term Assignment Group 1

## Group Members:


1. Student Name: Viral Maheshbhai Patel   
**Student ID: 8892496**

2. Student Name: Dhruv Mistry  
**Student ID: 8915299**

3. Student Name: Saumyakumar Vishnubhai Patel     
**Student ID: 8885180**

## Links

GitLab link https://gitlab.com/mid_term_group1/mid_term.git 


```sql
-- Database: OnlineBookstore

-- DROP DATABASE IF EXISTS "OnlineBookstore";

                    -- DDL Operations--

CREATE DATABASE "OnlineBookstore"
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;


CREATE TABLE Customer (
    customerId INT  PRIMARY KEY,
    customerName VARCHAR(20),
    customerEmail VARCHAR(50),
    customerPassword VARCHAR(20),
    customerTotalSpent DECIMAL(10,2),
    customerDateJoined DATE
);

CREATE TABLE Author (
    authorId INT  PRIMARY KEY,
    authorName VARCHAR(20),
    authorBiography TEXT
);


CREATE TABLE Publisher (
    publisherId INT  PRIMARY KEY,
    publisherName VARCHAR(20),
    publisherAddress VARCHAR(255)
);

CREATE TABLE Book (
    bookId INT  PRIMARY KEY,
    bookTitle VARCHAR(20),
    bookGenre VARCHAR(20),
    bookPublicationDate DATE,
    bookPrice DECIMAL(10,2),
    bookFormat VARCHAR(20),
    bookAuthorId INT,
    bookPublisherId INT,
    FOREIGN KEY (bookAuthorId) REFERENCES Author(authorId),
    FOREIGN KEY (bookPublisherId) REFERENCES Publisher(publisherId)
);

CREATE TABLE Review (
    reviewId INT  PRIMARY KEY,
    reviewBookId INT,
    reviewCustomerId INT,
    reviewRating INT,
    reviewComment TEXT,
    reviewDate DATE,
    FOREIGN KEY (reviewBookId) REFERENCES Book(bookId),
    FOREIGN KEY (reviewCustomerId) REFERENCES Customer(customerId)
);

CREATE TABLE Sale (
    saleId INT PRIMARY KEY,
    saleCustomerId INT,
    saleBookId INT,
    saleDate DATE,
    saleQuantity INT,
    saleTotalPrice DECIMAL(10,2),
    FOREIGN KEY (saleCustomerId) REFERENCES Customer(customerId),
    FOREIGN KEY (saleBookId) REFERENCES Book(bookId)
);

                    -- DML Operations --

INSERT INTO Author (authorId ,authorName, authorBiography) VALUES(101 , 'Amit Rupa','American novelist and short story writer' );
INSERT INTO Author (authorId ,authorName, authorBiography) VALUES(102 ,'Jaimin P',' which won the Pulitzer Prize.');
INSERT INTO Author (authorId ,authorName, authorBiography) VALUES(103 , 'Savani P', 'the Harry Potter fantasy series');
INSERT INTO Author (authorId ,authorName, authorBiography) VALUES(104 , 'Hiren M', 'English novelist');

INSERT INTO Publisher (publisherId,  publisherName, publisherAddress) VALUES
(1, 'Raj Van', '123 Main St, New York, NY 10001, USA'),
(2, 'Lippin Cott', '456 Elm St, Philadelphia, PA 19103, USA'),
(3,'Blooms Bury', '50 Bedford Square, London WC1B 3DP, UK'),
(4,'Secker P', '20 Vauxhall Bridge Rd, London SW1V 2SA, UK');

                    -- CRUD Operations --

Select * From publisher;

INSERT INTO Book (bookId ,bookTitle, bookGenre, bookPublicationDate, bookPrice, bookFormat, bookAuthorId, bookPublisherId)
VALUES (1 , 'Monk', 'Spiritual', '2010-04-10', 30, 'Physical', 101, 1);

INSERT INTO Book (bookId , bookTitle, bookGenre, bookPublicationDate, bookPrice, bookFormat, bookAuthorId, bookPublisherId)
VALUES (2, 'The 7 Habbits of why', 'Fiction', '2002-07-11', 15.55, 'E-book', 102, 2);


INSERT INTO Book (bookId, bookTitle, bookGenre, bookPublicationDate, bookPrice, bookFormat, bookAuthorId, bookPublisherId)
VALUES (3, 'Psyhology of money', 'lifehappiness', '2003-07-11', 20.60, 'E-book', 103, 3);


INSERT INTO Book (bookId , bookTitle, bookGenre, bookPublicationDate, bookPrice, bookFormat, bookAuthorId, bookPublisherId)
VALUES (4 , 'Secret', 'Documentry', '2013-07-11', 20.3, 'E-book', 104, 4);



UPDATE Book
SET bookPrice = 9.99
WHERE bookId = 1;

DELETE FROM Book
WHERE bookId = 2;

```

# Database Schema

### 1. Customer

| Attribute          | Type          |
|--------------------|---------------|
| customerId         | INT           |
| customerName       | VARCHAR(20)   |
| customerEmail      | VARCHAR(50)   |
| customerPassword   | VARCHAR(20)   |
| customerTotalSpent | DECIMAL(10,2) |
| customerDateJoined | DATE          |

### 2. Author

| Attribute       | Type        |
|-----------------|-------------|
| authorId        | INT         |
| authorName      | VARCHAR(20) |
| authorBiography | TEXT        |

### 3. Publisher

| Attribute        | Type         |
|------------------|--------------|
| publisherId      | INT          |
| publisherName    | VARCHAR(20)  |
| publisherAddress | VARCHAR(255) |

### 4. Book

| Attribute           | Type          |
|---------------------|---------------|
| bookId              | INT           |
| bookTitle           | VARCHAR(20)   |
| bookGenre           | VARCHAR(20)   |
| bookPublicationDate | DATE          |
| bookPrice           | DECIMAL(10,2) |
| bookFormat          | VARCHAR(20)   |
| bookAuthorId        | INT           |
| bookPublisherId     | INT           |

### 5. Review

| Attribute        | Type        |
|------------------|-------------|
| reviewId         | INT         |
| reviewBookId     | INT         |
| reviewCustomerId | INT         |
| reviewRating     | INT         |
| reviewComment    | TEXT        |
| reviewDate       | DATE        |

### 6. Sale

| Attribute       | Type          |
|-----------------|---------------|
| saleId          | INT           |
| saleCustomerId  | INT           |
| saleBookId      | INT           |
| saleDate        | DATE          |
| saleQuantity    | INT           |
| saleTotalPrice  | DECIMAL(10,2) |


## Blocks of code

```
//Typescript interface for Customer Table
interface Customer {
    customerId: number;
    customerName: string;
    customerEmail: string;
    customerPassword: string;
    customerTotalSpent: number;
    customerDateJoined: Date;
}

const customerExample: Customer = {
    customerId: 1,
    customerName: 'Jaimin Pandya',
    customerEmail: 'jpandya@example.com',
    customerPassword: 'password1803',
    customerTotalSpent: 150.75,
    customerDateJoined: new Date('2024-2-15')
};
console.log(customerExample);

//Typescript interface for Author Table
interface Author {
    authorId: number;
    authorName: string;
    authorBiography: string;
}

const authorExample: Author = {
    authorId: 101,
    authorName: 'F. Scott Fitzgerald',
    authorBiography: 'American novelist and short story writer'
};
console.log(authorExample);


//Typescript interface for Book Table
interface Book {
    bookId: number;
    bookTitle: string;
    bookGenre: string;
    bookPublicationDate: Date;
    bookPrice: number;
    bookFormat: string;
    bookAuthorId: number;
    bookPublisherId: number;
}

const bookExample: Book = {
    bookId: 1,
    bookTitle: 'Monk',
    bookGenre: 'Spiritual',
    bookPublicationDate: new Date('2010-04-10'),
    bookPrice: 30.00,
    bookFormat: 'Physical',
    bookAuthorId: 101,
    bookPublisherId: 1
};
console.log(bookExample);

```

