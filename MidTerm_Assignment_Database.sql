-- Database: OnlineBookstore

-- DROP DATABASE IF EXISTS "OnlineBookstore";

CREATE DATABASE "OnlineBookstore"
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;


CREATE TABLE Customer (
    customerId INT  PRIMARY KEY,
    customerName VARCHAR(20),
    customerEmail VARCHAR(50),
    customerPassword VARCHAR(20),
    customerTotalSpent DECIMAL(10,2),
    customerDateJoined DATE
);

CREATE TABLE Author (
    authorId INT  PRIMARY KEY,
    authorName VARCHAR(20),
    authorBiography TEXT
);


CREATE TABLE Publisher (
    publisherId INT  PRIMARY KEY,
    publisherName VARCHAR(20),
    publisherAddress VARCHAR(255)
);

CREATE TABLE Book (
    bookId INT  PRIMARY KEY,
    bookTitle VARCHAR(20),
    bookGenre VARCHAR(20),
    bookPublicationDate DATE,
    bookPrice DECIMAL(10,2),
    bookFormat VARCHAR(20),
    bookAuthorId INT,
    bookPublisherId INT,
    FOREIGN KEY (bookAuthorId) REFERENCES Author(authorId),
    FOREIGN KEY (bookPublisherId) REFERENCES Publisher(publisherId)
);

CREATE TABLE Review (
    reviewId INT  PRIMARY KEY,
    reviewBookId INT,
    reviewCustomerId INT,
    reviewRating INT,
    reviewComment TEXT,
    reviewDate DATE,
    FOREIGN KEY (reviewBookId) REFERENCES Book(bookId),
    FOREIGN KEY (reviewCustomerId) REFERENCES Customer(customerId)
);

CREATE TABLE Sale (
    saleId INT PRIMARY KEY,
    saleCustomerId INT,
    saleBookId INT,
    saleDate DATE,
    saleQuantity INT,
    saleTotalPrice DECIMAL(10,2),
    FOREIGN KEY (saleCustomerId) REFERENCES Customer(customerId),
    FOREIGN KEY (saleBookId) REFERENCES Book(bookId)
);



INSERT INTO Author (authorId ,authorName, authorBiography) VALUES(101 , 'F.Scott Fitzgerald','American novelist and short story writer' );
INSERT INTO Author (authorId ,authorName, authorBiography) VALUES(102 ,'Harper Lee',' which won the Pulitzer Prize.');
INSERT INTO Author (authorId ,authorName, authorBiography) VALUES(103 , 'J.K. Rowling', 'the Harry Potter fantasy series');
INSERT INTO Author (authorId ,authorName, authorBiography) VALUES(104 , 'George Orwell', 'English novelist');

INSERT INTO Publisher (publisherId,  publisherName, publisherAddress) VALUES
(1, 'Scribner', '123 Main St, New York, NY 10001, USA'),
(2, 'Lippincott', '456 Elm St, Philadelphia, PA 19103, USA'),
(3,'Bloomsbury', '50 Bedford Square, London WC1B 3DP, UK'),
(4,'Secker', '20 Vauxhall Bridge Rd, London SW1V 2SA, UK');

Select * From publisher;

INSERT INTO Book (bookId ,bookTitle, bookGenre, bookPublicationDate, bookPrice, bookFormat, bookAuthorId, bookPublisherId)
VALUES (1 , 'Monk', 'Spiritual', '2010-04-10', 30, 'Physical', 101, 1);

INSERT INTO Book (bookId , bookTitle, bookGenre, bookPublicationDate, bookPrice, bookFormat, bookAuthorId, bookPublisherId)
VALUES (2, 'The 7 Habbits of why', 'Fiction', '2002-07-11', 15.55, 'E-book', 102, 2);


INSERT INTO Book (bookId, bookTitle, bookGenre, bookPublicationDate, bookPrice, bookFormat, bookAuthorId, bookPublisherId)
VALUES (3, 'Psyhology of money', 'lifehappiness', '2003-07-11', 20.60, 'E-book', 103, 3);


INSERT INTO Book (bookId , bookTitle, bookGenre, bookPublicationDate, bookPrice, bookFormat, bookAuthorId, bookPublisherId)
VALUES (4 , 'Secret', 'Documentry', '2013-07-11', 20.3, 'E-book', 104, 4);



UPDATE Book
SET bookPrice = 9.99
WHERE bookId = 1;

DELETE FROM Book
WHERE bookId = 2;
